import * as React from "react";
import * as ReactDOM from "react-dom";
import { DialogueTree } from "./components/DialogueTree";
import dataLoader from './data';

ReactDOM.render(<DialogueTree dataLoader={dataLoader}></DialogueTree>, document.getElementById("root"));