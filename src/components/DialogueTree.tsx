import * as React from 'react';
import { DialogueNode } from './DialogueNode';
import { NodeData, Inventory, JsonOption, JsonNodeData } from '../NodeData';

interface LoaderOutput {
	nodes: JsonNodeData[];
	inventory: Inventory;
}

interface TreeProps {
	dataLoader: () => LoaderOutput;
}

// Stores the current node to render as well as the users current inventory
// If no current node then the game data has not been loaded yet
interface TreeState {
	currentNode?: NodeData;
	inventory: Inventory;
}

export class DialogueTree extends React.PureComponent<TreeProps, TreeState> {

	// uses dataLoader to get json data and convert it into node tree
	// sets first node in list to start node
	componentWillMount() {
		const nodes: NodeData[] = [];
		const allChoicesList: JsonOption[][] = [];

		const {nodes: nodeData, inventory} = this.props.dataLoader();
		nodeData.forEach(node => {
			const { choices, ...nodeBase } = node;
			nodes.push(Object.assign(nodeBase, { choices: [] }));
			allChoicesList.push(choices || []);
		});

		// relink nodes using choices list
		for (let i = 0; i < allChoicesList.length; i++) {
			const choiceList = allChoicesList[i];

			choiceList.forEach((choice) => {
				let { id, hideOnFailure, ...choiceBase } = choice;
				nodes[i].choices.push(
					Object.assign(choiceBase, {
						data: nodes[id], hideOnFailure: Boolean(hideOnFailure)
					}));
			});
		}

		this.setState({
			currentNode: nodes[0],
			inventory
		});
	}

	constructor(props) {
		super(props);
		this.state = { inventory: {} };
	}

	render() {
		let display = null;
		if (!this.state.currentNode) {
			display = <div>Loading</div>;
		} else {
			display =
				<DialogueNode
					data={this.state.currentNode}
					inventory={this.state.inventory}
					continue={this.chooseNewNode.bind(this)}>
				</DialogueNode>;
		}
		return (
			<div className="dialogue-tree">
				{display}
			</div>
		);
	}

	chooseNewNode(currentNode: NodeData) {
		this.setState((prev, props) => {
			let newInventory = Object.assign({}, prev.inventory);
			if (currentNode.loseOnEnter) {
				Object.keys(currentNode.loseOnEnter).forEach((item) => {
					if (newInventory[item]) {
						newInventory[item] -= currentNode.loseOnEnter[item];
						if (!newInventory[item] || newInventory[item] < 0) {
							delete newInventory[item];
						}
					}
				})
			}
			if (currentNode.acquireOnEnter) {
				Object.keys(currentNode.acquireOnEnter).forEach((item) => {
					if (newInventory[item]) {
						newInventory[item] += currentNode.acquireOnEnter[item];
					} else {
						newInventory[item] = currentNode.acquireOnEnter[item];
					}
				});
			}
			return {
				currentNode,
				inventory: newInventory
			};
		});
	}
}